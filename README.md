# docker-codeigniter3-base
Configuración inicial para codeigniter3 en php 7.4

## Requerimientos:
- instalar docker desde la pagina oficial: https://docs.docker.com/engine/install/ubuntu/ si es Ubuntu  
- hacer este post install para poder usar docker sin ser root https://docs.docker.com/engine/install/linux-postinstall/  
- instalar docker-compose desde la pagina oficial https://docs.docker.com/compose/install/  
- tener un proyecto codeigniter que se pondrá en el folder www

## configuracion inicial:
```
mkdir proyectoCI
cd proyectoCI
git clone git@gitlab.com:grodrigo.dev/docker-codeigniter3-base.git .
# en el folder www ponemos nuestro codeigniter
git clone git@gitlab.com:MinisteriodDeProduccionDelegacion/catalogoTC.git www
```

## cambiar estas cosas dentro del proyecto en www:
en file 
`application/config/config.php`
agregar al final las lineas
```
$config['base_url'] = 'http://localhost:8080';
$config['sess_save_path'] = sys_get_temp_dir();
```

cambiar la configuracion de la base de datos, en particular fijarse el hostname, username, password, database.  
File `application/config/database.php`

```
        'hostname' => 'mysql',
        'username' => 'dbuser',
        'password' => 'secret',
        'database' => 'catalogoTC',
```

Revisar el puerto de apache y mysql en el file docker-compose.yml (del lado izquierdo son los de nuestra maquina,
del derecho los del container, no se pueden estar ya usados en nuestra pc los de la izquierda, sino elegir otro) y hacer  
`docker-compose up -d`

## para importar la db:
### metodo 1
usar mysqlworkbench y conectarse en el puerto exportado por el servicio mysql del docker  
ejemplo usar `localhost:3306`

### metodo 2
importar por consola
```
sudo chown -R $USER:$USER backups/
copiar el dump en el folder backups en la base del proyecto
# entrar en el container
docker exec -it tc-app bash
# ahora estamos dentro del container corriendo bash
cd /backups # este folder esta mapeado en nuestra maquina, lo que ponemos ahi lo vemos en el container
mysql -u dbuser -psecret catalogoTC < catalogoTC.sql
# salir del contenedor
exit
```
### metodo 3
usar el phpmyadmin proveido desde el docker-compose e importar desde browser

### notas
Ver en el browser   
`localhost:8080`

Para entrar en el bash del container de tc-app:  
`docker exec -it tc-app bash`

Para reiniciar el servicio app
`docker restart tc-app`
o
`docker-compose restart app`

Para ver los servicios corriendo:
docker-compose ps # muestra los servicios activos del stack
docker ps # muestra los containers corriendo de la pc

El docker-compose brinda 3 servicios, app, mysql y phpmyadmin.  
Para correr comandos en los containers se puede hacer de dos formas, con docker-compose o con docker, es lo mismo.  
Si usás el comando docker-compose se los debe llamar por nombre de servicio, ej `docker-compose exec app bash`  
Si usás el comando docker tenés que llamar con los nombres de los containers, ej `docker exec -it tc-app bash`  
