FROM php:7.4-apache

# copy apache configuration due to laravel uses public folder
COPY etc/apache2/sites-available /etc/apache2/sites-available

RUN apt update && \
    apt install -y git libxml2-dev libpng-dev libc-client-dev libkrb5-dev libpq-dev libzip-dev locales libbz2-dev

RUN rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin

# install PHP extensions
RUN docker-php-ext-install soap pdo_mysql mysqli exif pcntl bcmath intl zip gd bz2

#build locales
RUN echo " es_AR.UTF-8 UTF-8">> /etc/locale.gen && locale-gen

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Enable mod-rewrite
RUN a2enmod rewrite && service apache2 restart

EXPOSE 80

CMD ["apache2-foreground"]

